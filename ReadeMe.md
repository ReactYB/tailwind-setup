// Step 0 
https://www.microverse.org/blog/configure-tailwind-css-for-vanilla-js-with-jit-in-10-easy-steps

// Step 1
$ mkdir tailwind-latest

// Step 2
$ npm init

// Step 3
$ npm install -D tailwindcss@latest autoprefixer postcss-cli@latest

// Step 4
This is where you will insert the Tailwind directives to generate the CSS from the Tailwind components.
@tailwind base;
@tailwind components;
@tailwind utilities;

// Step 5
Create HTML

// Step 6
To generate this and the Postcss config files automatically, run  $ npx tailwindcss init -p .

// Step 7 
Configurer Jit dans tailwind.config.js

// Step 8
These custom scripts that I wrote, utilize concurrently, an npm package that allows multiple scripts to run at the same time. In this case, we are running both Webpack and Postcss simultaneously.

In the first instance, running  $ npm run start-dev would trigger  $ npm run dev to call Webpack to run in the development environment. It would also trigger  $ npm css-dev  to make sure Tailwind is being watched, with the compiled styles outputted in  src/styles.css .

In the same manner, run  $ npm run start-prod  when the app is ready for production. It triggers  $ npm run prod  to ensure that the environment is set to production mode and  $ npm run css-prod to trigger Postcss compilation with the final classes being utilized.

// Step 9
